package utils

import (
	"os"
	"os/exec"
)

func Exec(silent bool,cmd string, arg ...string) (int, error){


	var args = []string{cmd}
	for _, k := range arg {
		args = append(args, k)
	}

	c := exec.Command(cmd)
	c.Args = args
	if !silent {
		c.Stdout = os.Stdout
		c.Stdin = os.Stdin
		c.Stderr = os.Stderr
	}
	err := c.Run()
	return c.ProcessState.ExitCode(), err
}

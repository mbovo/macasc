package utils

import (
	"github.com/google/martian/log"
	"os"
	"path/filepath"
	"strings"
)

func FileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func ExistsInPath(filename string) bool {
	for _, v := range strings.Split(os.Getenv("PATH"), ":"){
		match, err := filepath.Glob( filepath.Join(v,"brew"))
		if err != nil {  log.Errorf("%v\n", err)}
		if len(match) > 0 { return true }
	}
	return false
}


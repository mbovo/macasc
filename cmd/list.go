package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// listCmd represents the list command
var listCmd = &cobra.Command{
	Use:   "list",
	Short: "List submodules",
	Long:  `List all available submodules. A Submodules is independent installable unit`,
	Run:   listMe,
}

func init() {
	rootCmd.AddCommand(listCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// listCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// listCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

func listMe(cmd *cobra.Command, args []string) {
	for _, c := range rootCmd.Commands() {
		if c.Name() != "list" && c.Name() != "help" {
			fmt.Printf("%s\t\t%s\n", c.Name(), c.Short)
		}
	}
}

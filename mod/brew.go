package mod

import (
	"fmt"
	"github.com/mbovo/macasc/utils"
	"io/ioutil"
	"errors"
	"gopkg.in/yaml.v2"
)

type Brew struct {
	Formulae []string
}

func NewBrew() *Brew{
	return &Brew{}
}

func BrewFromFile(filepath string) (*Brew, error){
	b := NewBrew()
	if ! utils.FileExists(filepath){
		return nil, errors.New("file not found")
	}
	f, err := ioutil.ReadFile(filepath)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(f, b)
	if err != nil {
		fmt.Printf("ERROR: cannot unmarshal data %v",err)
	}
	return b, err
}

func BrewFromUrl(url string) (*Brew, error){
	b := NewBrew()
	f, err := utils.Download(url)
	if err != nil {
		return nil, err
	}
	err = yaml.Unmarshal(f, b)
	return b, err
}

func (*Brew) Install() {

	fmt.Println("+ Downloading brew")
	if err := utils.DownloadToFile("https://raw.githubusercontent.com/Homebrew/install/master/install.sh", "/tmp/brew_install.sh", 0744); err != nil {
		panic(err)
	}

	fmt.Println("+ Installing brew")
	if _, err := utils.Exec(false, "/bin/bash", "-c", "/tmp/brew_install.sh"); err != nil {
		panic(err)
	}

}

func (b *Brew) Configure(upgrade bool){

	if !upgrade {
		b.filterFormulae()
	}
	if _, err := b.preFetch(); err != nil {
		fmt.Printf("Error prefetching formulas: %v", err)
	}

	for _, formula := range b.Formulae {
		if upgrade {
			fmt.Println("++ Trying upgrade")
			upgradeFormula(formula)
		}else{
			fmt.Printf("++ Installing %s \n", formula)
			installFormula(formula)
		}
	}
}

func (b *Brew) preFetch() (int, error) {
	s := []string{"fetch"}
	if len(b.Formulae) < 1 {
		return 0,nil
	}
	for _, i := range b.Formulae  {
		s = append(s, i)
	}
	fmt.Printf("+ Pre-fetching %d formulas\n", len(b.Formulae))
	return utils.Exec(true, "/usr/local/bin/brew", s...)
}

func (b *Brew) filterFormulae(){
	fmt.Printf("+ Check %d formulae\t", len(b.Formulae))
	c := make(chan string, len(b.Formulae)+1)
	for _, f := range b.Formulae{
		go existsFormula(f, c)
	}
	var l []string
	for _ = range b.Formulae{
		s := <-c
		if s != ""{
			l = append(l, s )
		}
	}
	fmt.Printf("%d missing\n", len(l))
	b.Formulae = l
}

func installFormula(formula string) (int, error) {
	return utils.Exec(false, "/usr/local/bin/brew", "install", formula)

}

func upgradeFormula(formula string) (int, error) {
	return utils.Exec(false, "/usr/local/bin/brew", "upgrade", formula )
}

func existsFormula(formula string, c chan string) {
	rc, err := utils.Exec(true, "/usr/local/bin/brew", "list", formula)
	if rc != 0 || err != nil {
		c <- formula
	}
	c <- ""
}

func removeFormula(formula string){
	utils.Exec(false, "/usr/local/bin/brew", "uninstall", formula)
}

func (b *Brew) Remove() {
	for _, formula := range b.Formulae {
		fmt.Printf("++ Uninstalling %s \n", formula)
		removeFormula(formula)
	}
}

func (b *Brew) Verify() {
	fmt.Print("+ Checking brew\t")
	if !utils.FileExists("/usr/local/bin/brew") &&
		!utils.ExistsInPath("brew") {
		fmt.Println("KO\tBrew is not installed!")
	} else {
		fmt.Println("OK")
		for _, formula := range b.Formulae {
			var v string
			//if existsFormula(formula) {
			//	v = "YES"
			//} else {
			//	v = "NO"
			//}
			fmt.Printf("++ Verifying formula: %s\t\t\t%s\n", formula, v)
		}
	}
}

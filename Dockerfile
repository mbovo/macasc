FROM golang:1.14 as builder

RUN mkdir /app
WORKDIR /app
ADD . /app/
RUN make build


FROM scratch
COPY --from=builder /app/bin/macasc /macasc
ENTRYPOINT ["/macasc"]

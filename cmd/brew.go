/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"github.com/google/martian/log"
	"github.com/spf13/cobra"
	"github.com/mbovo/macasc/mod"
	"strings"
)

// brewCmd represents the brew command
var brewCmd = &cobra.Command{
	Use:   "brew",
	Short: "Dealing with HomeBrew, package system for mac",
	Long: `long`,
	Run: func(cmd *cobra.Command, args []string) {
		cmd.Help()
	},
}

var configureBrewCmd = &cobra.Command{
	Use:   "configure",
	Short: "Configure brew",
	Long: `lkong`,
	Run: func(cmd *cobra.Command, args []string) {
		uri := cmd.Flag("path").Value.String()
		var b *mod.Brew
		if uri != "" {
			var e error
			if strings.Contains(uri, "http"){
				b, e = mod.BrewFromUrl(uri)
			}else{
				b, e = mod.BrewFromFile(uri)
			}
			if e != nil {
				log.Errorf("Error loading %s : %s\n", uri, e.Error() )
			}
		}else{
			b = mod.NewBrew()
		}
		b.Configure(false)
	},
}

var installBrewCmd = &cobra.Command{
	Use:   "install",
	Short: "Install homebrew on the system",
	Long: `long`,
	Run: func(cmd *cobra.Command, args []string) {
		b := mod.NewBrew()
		b.Install()
	},
}

var removeBrewCmd = &cobra.Command{
	Use:   "remove",
	Short: "Remove homebrew from the system",
	Long: `long`,
	Run: func(cmd *cobra.Command, args []string) {
		uri := cmd.Flag("path").Value.String()
		var b *mod.Brew
		if uri != "" {
			var e error
			if strings.Contains(uri, "http"){
				b, e = mod.BrewFromUrl(uri)
			}else{
				b, e = mod.BrewFromFile(uri)
			}
			if e != nil {
				log.Errorf("Error loading %s : %s\n", uri, e.Error() )
			}
		}else{
			b = mod.NewBrew()
		}
		b.Remove()
	},
}

var verifyBrewCmd = &cobra.Command{
	Use:   "verify",
	Short: "Verify if homebrew is (and relative formulae are) installed",
	Long: `longText`,
	Run: func(cmd *cobra.Command, args []string) {
		b := mod.NewBrew()
		b.Verify()
	},
}

var upgradeBrewCmd = &cobra.Command{
	Use:   "upgrade",
	Short: "Upgrade homebrew formulas",
	Long: `long`,
	Run: func(cmd *cobra.Command, args []string) {
		uri := cmd.Flag("path").Value.String()
		var b *mod.Brew
		if uri != "" {
			var e error
			if strings.Contains(uri, "http"){
				b, e = mod.BrewFromUrl(uri)
			}else{
				b, e = mod.BrewFromFile(uri)
			}
			if e != nil {
				log.Errorf("Error loading %s : %s\n", uri, e.Error() )
			}
		}else{
			b = mod.NewBrew()
		}
		b.Configure(true)
	},
}

func init() {
	rootCmd.AddCommand(brewCmd)
	brewCmd.AddCommand(configureBrewCmd)
	brewCmd.AddCommand(installBrewCmd)
	brewCmd.AddCommand(removeBrewCmd)
	brewCmd.AddCommand(verifyBrewCmd)
	brewCmd.AddCommand(upgradeBrewCmd)
	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// brewCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// brewCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	configureBrewCmd.Flags().StringP("path", "p", "", "Config file path or remote url")
	upgradeBrewCmd.Flags().StringP("path", "p", "", "Config file path or remote url")
	removeBrewCmd.Flags().StringP("path", "p", "", "Config file path or remote url")
}
